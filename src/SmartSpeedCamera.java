package com.sjmgilburt.cloudcomputing;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.Date;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.microsoft.windowsazure.Configuration;
import com.microsoft.windowsazure.services.servicebus.*;
import com.microsoft.windowsazure.services.servicebus.models.*;
import com.microsoft.windowsazure.core.*;
import com.microsoft.windowsazure.exception.ServiceException;

import javax.xml.datatype.*;

public class SmartSpeedCamera {
	/** Class variables */
	private int id;
	private String street;
	private String city;
	private int maxMPH;
	private Date startTime;
	
	private Queue<SSCMessage> outBuffer;
	
	private static final String SERVICE_BUS = "sjmg-cloudcomputing";
	private static final String TOPIC = "camera-message";
	private static final String PRIMARY_KEY = "5Aatn3q0TNeggXzwxlHACyZc3D6C8v1gZ/+Ja3bxRD8=";
	private static final String CONNECTION_URL = "http://sjmg-cloudcomputing.servicebus.windows.net";
	
	/** Constructors */
	public SmartSpeedCamera(String configPath) {
		startTime = new Date();
		
		try {
			FileReader fr;
			fr = new FileReader(configPath);
			BufferedReader br = new BufferedReader(fr);
			
			id = Integer.valueOf(br.readLine());
			street = br.readLine();
			city = br.readLine();
			maxMPH = Integer.valueOf(br.readLine());
			
			br.close();
			fr.close();		
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		System.out.println("Camera constructed."); // Debug line
		
		outBuffer = new LinkedList<SSCMessage>();
		outBuffer.add(new SSCMessage(id, street, city, maxMPH, startTime));
		send();
	}
	
	/** Getters and setters */
	public int getID() { 
		return id;
	}
	
	public String getStreet() {
		return street;
	}
	
	public String getCity() {
		return city;
	}
	
	public int getMaxMPH() {
		return maxMPH;
	}
	
	public Date getStartTime() {
		return startTime;
	}
	
	public void setStreet(String street) {
		this.street = street;
	}
	
	public void setCity(String city) {
		this.city = city;
	}
	
	/** Methods */
	public void vehiclePass() {
		Vehicle veh = new Vehicle(maxMPH, id);
		SSCMessage msg = new SSCMessage(id, street, city, maxMPH, startTime, veh);
		outBuffer.add(msg);
		send();
	}
	
	void send() {
		if (!connection()) {
			System.out.println("Message stored."); // Debug line
			return;
		}
		else System.out.println("Connection verified."); // Debug line
		
		// Attempt to send all SSCMessages in the queue to Azure
		Configuration config = 
				ServiceBusConfiguration.configureWithSASAuthentication(
			      SERVICE_BUS,
			      "RootManageSharedAccessKey",
			      PRIMARY_KEY,
			      ".servicebus.windows.net"
			      );

		ServiceBusContract service = ServiceBusService.create(config);
			
		try {
			while (!outBuffer.isEmpty()) {
				SSCMessage sscm = outBuffer.remove();
				
				Gson gson = new GsonBuilder().create();
			    BrokeredMessage message = new BrokeredMessage(gson.toJson(sscm));
			    
			    if (sscm.hasVehicle() && (sscm.vehicle.speed > sscm.maxMPH)) {
			    	message.setProperty("Speeding", true);
			    }
			    else message.setProperty("Speeding", false);
			    
			    service.sendTopicMessage(TOPIC, message);
			    System.out.println("Message sent."); // Debug line
			}
		}
		catch (ServiceException e) {
			System.out.print("ServiceException encountered: ");
		    System.out.println(e.getMessage());
		    System.exit(-1);
		}
	}
	
	private boolean connection() {
		try {
            URL url = new URL(CONNECTION_URL);
            HttpURLConnection urlConnect = (HttpURLConnection)url.openConnection();
            
            urlConnect.getContent(); // Will throw exception if no connection
        } catch (UnknownHostException e) {
            e.printStackTrace();
            return false;
        }
        catch (IOException e) {
            e.printStackTrace();
            return false;
        }
		return true;
	}
	
	/** Main method */
	public static void main( String[] args ) {
		Scanner in = new Scanner(System.in);
		
		System.out.print("Config file (path): ");
		String path = in.next();
		System.out.print("Vehicles per minute: ");
		double rate = in.nextDouble();
		
		SmartSpeedCamera ssc = new SmartSpeedCamera(path);
		in.close();
		
		while (true) {
			try {
				ssc.vehiclePass();
				Thread.sleep((int)(60000/rate));
			}
			catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
    }
}
