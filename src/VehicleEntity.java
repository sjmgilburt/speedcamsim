package com.sjmgilburt.cloudcomputing;

import java.util.Date;

import com.sjmgilburt.cloudcomputing.Vehicle.Type;
import com.microsoft.azure.storage.*;
import com.microsoft.azure.storage.table.*;
import com.microsoft.azure.storage.table.TableQuery.*;

public class VehicleEntity extends TableServiceEntity {
	private String reg;	
	private String type; // Vehicle.Type not allowed
	private double speed;
	private Date dateTime;
	private int sscID;
	
	public VehicleEntity() {} // Nullary constructor required
	
	VehicleEntity(Vehicle veh) {
		switch (veh.type) {
			case CAR:			this.partitionKey = "CAR";
								break;
			case MOTORCYCLE:	this.partitionKey = "MOTORCYCLE";
								break;
			case TRUCK: 		this.partitionKey = "TRUCK";
								break;
		}
        this.rowKey = veh.reg + ", " + veh.dateTime.toString();
        
        this.reg = veh.reg;
        this.type = veh.type.toString();
        this.speed = veh.speed;
        this.dateTime = veh.dateTime;
        this.sscID = veh.sscID;
	}
	
	VehicleEntity(String reg, Vehicle.Type type, double speed, Date dateTime, int cameraID) {
		switch (type) {
			case CAR:			this.partitionKey = "CAR";
								break;
			case MOTORCYCLE:	this.partitionKey = "MOTORCYCLE";
								break;
			case TRUCK: 		this.partitionKey = "TRUCK";
								break;
		}
        this.rowKey = reg + ", " + dateTime.toString();
        
        this.reg = reg;
        this.type = type.toString();
        this.speed = speed;
        this.dateTime = dateTime;
        this.sscID = cameraID;
	}
	
	public String getReg() {
		return reg;
	}
	
	public String getType() {
		return type;
	}
	
	public double getSpeed() {
		return speed;
	}
	
	public Date getDateTime() {
		return dateTime;
	}
	
	public int getSSCID() {
		return sscID;
	}
	
	public void setReg(String reg) {
		this.reg = reg;
	}
	
	public void setType(String type) {
		this.type = type;
	}
	
	public void setSpeed(double speed) {
		this.speed = speed;
	}
	
	public void setDateTime(Date dateTime) {
		this.dateTime = dateTime;
	}
	
	public void setSSCID(int sscID) {
		this.sscID = sscID;
	}
}
