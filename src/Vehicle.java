package com.sjmgilburt.cloudcomputing;

import java.util.Date;
import java.util.Random;

public class Vehicle {
	public String reg;	
	public enum Type {CAR, TRUCK, MOTORCYCLE}
	public Type type;
	public double speed;
	public Date dateTime;
	public int sscID;
	
	public Vehicle(int speedLimit, int cameraID) {
		reg = regGenerator();
		
		Random rn = new Random();
		int i = rn.nextInt(3);
		switch (i) {
			case 0:	type = Type.CAR;
					break;
			case 1:	type = Type.MOTORCYCLE;
					break;
			case 2: type = Type.TRUCK;
					break;
		}
		
		speed = rn.nextGaussian()*2.0 + speedLimit; // Estimate with Gaussian average at speed limit, SD equals 2.0 
		
		dateTime = new Date();
		sscID = cameraID;
	}
	
	public String toString() {
		return "[ " + reg +
				", " + type.toString() +
				", " + speed +
				", " + dateTime.toString() +
				", " + sscID + " ]";
	}
	
	private String regGenerator() {
		String result = "";
		
		String l2 = "ABCDEFGHJKLMNOPRSTUVWXY";
		
		Random rn = new Random();
		if (rn.nextFloat() > 0.2) { // Est. 80% chance vehicle was registered since Sep 2001
			String l1 = "ABCDEFGHKLMNOPRSVWXY";
			result += l1.charAt(rn.nextInt(l1.length()));
			result += l2.charAt(rn.nextInt(l2.length()));
			
			int year = rn.nextInt(15)+1;
			if ((year == 1) || (rn.nextBoolean())) {
				year += 50;
				result += year;
			}
			else if (year < 10) {result += "0" + year;}
			else result += year;
		}
		else {
			String l1 = "YXWVTSRPNMLKJHGFEDCBA";
			try {
				double chance = Math.abs(rn.nextGaussian()*5); // Decreasing chance with age
				result += l1.charAt((int)chance);
			} catch (StringIndexOutOfBoundsException e) {
				result += "Y"; // Slight chance of Gaussian choosing value larger than l1.length()
			}
			
			result += (rn.nextInt(979)+21);
		}
		
		result += " " + l2.charAt(rn.nextInt(l2.length()))
				+ l2.charAt(rn.nextInt(l2.length()))
				+ l2.charAt(rn.nextInt(l2.length()));
		
		return result;
	}
	
}
