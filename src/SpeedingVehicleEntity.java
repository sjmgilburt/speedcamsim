package com.sjmgilburt.cloudcomputing;

import java.util.Date;

public class SpeedingVehicleEntity extends VehicleEntity {
	private boolean priority;
	
	public SpeedingVehicleEntity() {}
	
	SpeedingVehicleEntity(Vehicle veh, boolean priority) {
		switch (veh.type) {
			case CAR:			this.partitionKey = "CAR";
								break;
			case MOTORCYCLE:	this.partitionKey = "MOTORCYCLE";
								break;
			case TRUCK: 		this.partitionKey = "TRUCK";
								break;
		}
        this.rowKey = veh.reg + ", " + veh.dateTime.toString();
        
        this.setReg(veh.reg);
        this.setType(veh.type.toString());
        this.setSpeed(veh.speed);
        this.setDateTime(veh.dateTime);
        this.setSSCID(veh.sscID);
        
        this.priority = priority;
	}
	
	SpeedingVehicleEntity(String reg, Vehicle.Type type, double speed, Date dateTime, int cameraID, boolean priority) {
		switch (type) {
			case CAR:			this.partitionKey = "CAR";
								break;
			case MOTORCYCLE:	this.partitionKey = "MOTORCYCLE";
								break;
			case TRUCK: 		this.partitionKey = "TRUCK";
								break;
		}
        this.rowKey = reg + ", " + dateTime.toString();
        
        this.setReg(reg);
        this.setType(type.toString());
        this.setSpeed(speed);
        this.setDateTime(dateTime);
        this.setSSCID(cameraID);
        
        this.priority = priority;
	}
	
	public boolean getPriority() {
		return priority;
	}
	
	public void setPriority(boolean priority) {
		this.priority = priority;
	}
}
