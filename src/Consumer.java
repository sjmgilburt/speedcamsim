package com.sjmgilburt.cloudcomputing;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URISyntaxException;
import java.security.InvalidKeyException;
import java.util.LinkedList;

import com.google.gson.Gson;
import com.microsoft.azure.storage.*;
import com.microsoft.azure.storage.table.*;
import com.microsoft.azure.storage.table.TableQuery.*;
import com.microsoft.windowsazure.Configuration;
import com.microsoft.windowsazure.services.servicebus.*;
import com.microsoft.windowsazure.services.servicebus.models.*;
import com.microsoft.windowsazure.core.*;
import com.microsoft.windowsazure.exception.ServiceException;

import javax.xml.datatype.*;

public class Consumer {
	private static final String SERVICE_BUS = "sjmg-cloudcomputing";
	private static final String TOPIC = "camera-message";
	private static final String SUBSCRIPTION = "msg-subscription";
	private static final String SERVICE_PRIMARY_KEY = "5Aatn3q0TNeggXzwxlHACyZc3D6C8v1gZ/+Ja3bxRD8=";
	private static final String STORAGE_ACCOUNT = "resourcegp7343";
	private static final String STORAGE_KEY = "b4XhY4AAaWwMvwcCn0mKogYI44tvhig7ClW7K69ObV7qMlMz/80QKKFBowJfEwzkNlXg+cghNhbOSHmm7QkvoA==";
	private static final String STORAGE_CONNECTION_STRING = 
		    "DefaultEndpointsProtocol=http;" + 
		    "AccountName="+STORAGE_ACCOUNT+";" +
		    "AccountKey="+STORAGE_KEY;
	
	private static final String CAMERA_TABLE_NAME = "cameras";
	private static final String VEHICLE_TABLE_NAME = "vehicles";
	
	// Poll values can be altered - suggestions below:
	private static final int INITIAL_POLL_INTERVAL = 10000; // 10000 one minute
	private static final int MAX_POLL_INTERVAL = 300000; // 300000 5 minutes
	private static final int MIN_POLL_INTERVAL = 1000; // 1000 one second
	private static final double INTERVAL_INCREASE_FACTOR = 1.5; // 1.5
	private static final double INTERVAL_DECREASE_FACTOR = 0.5; // 0.5
	private static int pollInterval;
	
	private static LinkedList<SSCMessage> messages = new LinkedList<SSCMessage>();
	
	private static void createTables(CloudTableClient client) {
		// Will not overwrite tables, only creates if not already created
		try {	
			CloudTable cameras = client.getTableReference(CAMERA_TABLE_NAME);
			cameras.createIfNotExists();
			
			CloudTable vehicles = client.getTableReference(VEHICLE_TABLE_NAME);
			vehicles.createIfNotExists();
		}
		catch (Exception e) {
		    e.printStackTrace();
		}
	}
	
	private static LinkedList<SSCMessage> poll() {
		Configuration config =
			    ServiceBusConfiguration.configureWithSASAuthentication(
			      SERVICE_BUS,
			      "RootManageSharedAccessKey",
			      SERVICE_PRIMARY_KEY,
			      ".servicebus.windows.net"
			      );
		ServiceBusContract service = ServiceBusService.create(config);
		
		LinkedList<SSCMessage> result = new LinkedList<SSCMessage>();
		
		try {
			ReceiveMessageOptions opts = ReceiveMessageOptions.DEFAULT;
			
			while(true) {
				ReceiveSubscriptionMessageResult resultSubMsg = service.receiveSubscriptionMessage(TOPIC, SUBSCRIPTION, opts);
				BrokeredMessage message = resultSubMsg.getValue();
				
				if (message != null && message.getMessageId() != null) {
					System.out.println("MessageID: " + message.getMessageId()); // Debug line
					
					String json = "";
					try {
						Reader inStrRd = new InputStreamReader(message.getBody());
						int data = -1;
						data = inStrRd.read();
						while (data != -1) {
							json += (char)data;
							data = inStrRd.read();
						}
					}
					catch (IOException e) {
						e.printStackTrace();
					}
					
					Gson gson = new Gson();
					SSCMessage sscm = gson.fromJson(json, SSCMessage.class);
					result.add(sscm);
					
					System.out.println("SSCMessage: " + sscm.toString()); // Debug line
				}
				else {
					System.out.println("No more messages."); // Debug line
					break;
				}
			}
		}
		catch (ServiceException e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	private static void addToTables(CloudTableClient client) {
		CloudTable camTable = null;
		CloudTable vehTable = null;
		try {
			camTable = client.getTableReference(CAMERA_TABLE_NAME);
			vehTable = client.getTableReference(VEHICLE_TABLE_NAME);
		}
		catch (URISyntaxException e) {
			e.printStackTrace();
		}
		catch (StorageException e) {
			e.printStackTrace();
		}
		
		TableBatchOperation carBatch = new TableBatchOperation();
		TableBatchOperation bikeBatch = new TableBatchOperation();
		TableBatchOperation truckBatch = new TableBatchOperation();
		try {
			for (SSCMessage sscm : messages) {
				if (!sscm.hasVehicle()) {
					CameraEntity entity = new CameraEntity(sscm);
					camTable.execute(TableOperation.insertOrReplace(entity));
				}
				else {
					VehicleEntity entity = new VehicleEntity(sscm.vehicle);
					if (entity.getType().equals("CAR"))	carBatch.insertOrReplace(entity);
					else if (entity.getType().equals("MOTORCYCLE")) bikeBatch.insertOrReplace(entity);
					else truckBatch.insertOrReplace(entity);
				}
			}
			if (!carBatch.isEmpty()) vehTable.execute(carBatch);
			if (!bikeBatch.isEmpty()) vehTable.execute(bikeBatch);
			if (!truckBatch.isEmpty()) vehTable.execute(truckBatch);
		}
		catch (StorageException e) {
			e.printStackTrace();
		}
		
		messages = new LinkedList<SSCMessage>();
	}

	public static void main(String[] args) {
		CloudStorageAccount storageAccount = null;
		CloudTableClient tableClient = null;
		try {
			storageAccount = CloudStorageAccount.parse(STORAGE_CONNECTION_STRING);
			tableClient = storageAccount.createCloudTableClient();
		
			createTables(tableClient);
		}
		catch (InvalidKeyException e) {
			e.printStackTrace();
		}
		catch (URISyntaxException e) {
			e.printStackTrace();
		}
		
		pollInterval = INITIAL_POLL_INTERVAL;
		while (true) {
			messages = poll();
			
			if (messages.isEmpty()) {
				pollInterval *= INTERVAL_INCREASE_FACTOR;
				if (pollInterval > MAX_POLL_INTERVAL) pollInterval = MAX_POLL_INTERVAL;
			}
			else {
				addToTables(tableClient);
				pollInterval *= INTERVAL_DECREASE_FACTOR;
				if (pollInterval < MIN_POLL_INTERVAL) pollInterval = MIN_POLL_INTERVAL;
			}
			try {
				Thread.sleep(pollInterval);
			}
			catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

}
