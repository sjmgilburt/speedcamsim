package com.sjmgilburt.cloudcomputing;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URISyntaxException;
import java.security.InvalidKeyException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;

import com.google.gson.Gson;
import com.microsoft.azure.storage.CloudStorageAccount;
import com.microsoft.azure.storage.StorageException;
import com.microsoft.azure.storage.queue.CloudQueue;
import com.microsoft.azure.storage.queue.CloudQueueClient;
import com.microsoft.azure.storage.queue.CloudQueueMessage;
import com.microsoft.azure.storage.table.CloudTableClient;
import com.microsoft.windowsazure.Configuration;
import com.microsoft.windowsazure.exception.ServiceException;
import com.microsoft.windowsazure.services.servicebus.ServiceBusConfiguration;
import com.microsoft.windowsazure.services.servicebus.ServiceBusContract;
import com.microsoft.windowsazure.services.servicebus.ServiceBusService;
import com.microsoft.windowsazure.services.servicebus.models.BrokeredMessage;
import com.microsoft.windowsazure.services.servicebus.models.ReceiveMessageOptions;
import com.microsoft.windowsazure.services.servicebus.models.ReceiveSubscriptionMessageResult;

import java.sql.*;

import com.microsoft.sqlserver.jdbc.*;

public class VehicleCheck {
	private static final String STORAGE_ACCOUNT = "resourcegp7343";
	private static final String STORAGE_KEY = "b4XhY4AAaWwMvwcCn0mKogYI44tvhig7ClW7K69ObV7qMlMz/80QKKFBowJfEwzkNlXg+cghNhbOSHmm7QkvoA==";
	private static final String STORAGE_CONNECTION_STRING = 
		    "DefaultEndpointsProtocol=http;" + 
		    "AccountName="+STORAGE_ACCOUNT+";" +
		    "AccountKey="+STORAGE_KEY;
	private static final String QUEUE_NAME = "scalable-queue";
	private static final String PWORD = "acses-987";
	
	private static LinkedList<String> resultsSQL;
	
	private static boolean isVehicleStolen(Vehicle veh) {
		try {
			Thread.sleep(5000);
		}
		catch (InterruptedException e) {
			e.printStackTrace();
		}
		return (Math.random() > 0.95);
	}

	private static void persistData() {
		String connectionString = "jdbc:sqlserver://zirm8u53ce.database.windows.net:1433;"
								+ "database=sql-database;"
								+ "user=admin-login@zirm8u53ce;"
								+ "password="+PWORD+";"
								+ "encrypt=true;"
								+ "hostNameInCertificate=*.database.windows.net;"
								+ "loginTimeout=30;";
		
		if (!resultsSQL.isEmpty()) {
			try {
				Connection connection = DriverManager.getConnection(connectionString);
				
				String insertSQL = "INSERT INTO Checks (reg, stolen) VALUES ";
				int iter = resultsSQL.size();
				for (int i=0; i<iter; i++) {
					if(resultsSQL.size() == 1) {
					    insertSQL += resultsSQL.poll() + ";";
					}
					else insertSQL += resultsSQL.poll() + ", ";
				}
				PreparedStatement insert = connection.prepareStatement(insertSQL);
				
				insert.execute();
				
			}
			catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	public static void main(String[] args) {
		while(true) {
			resultsSQL = new LinkedList<String>();
			try {
				CloudStorageAccount storageAccount = CloudStorageAccount.parse(STORAGE_CONNECTION_STRING);
				CloudQueueClient queueClient = storageAccount.createCloudQueueClient();
				CloudQueue queue = queueClient.getQueueReference(QUEUE_NAME);
				while(true) {
					CloudQueueMessage message = queue.retrieveMessage();
									
					if (message != null && message.getMessageId() != null) {
						System.out.println("MessageID: " + message.getMessageId()); // Debug line
						
						String json = message.getMessageContentAsString();
						
						Gson gson = new Gson();
						SSCMessage sscm = gson.fromJson(json, SSCMessage.class);
						
						if (sscm.hasVehicle()) {
							int bool = isVehicleStolen(sscm.vehicle) ? 1 : 0; // Converts boolean result to 1/0
							resultsSQL.add("('" + sscm.vehicle.reg + "', " + bool + ")");
						}
						
						queue.deleteMessage(message);
					}
					else {
						System.out.println("No more messages."); // Debug line
						break;
					}
				}
			}
			catch (StorageException e) {
				e.printStackTrace();
			} catch (InvalidKeyException e) {
				e.printStackTrace();
			} catch (URISyntaxException e) {
				e.printStackTrace();
			}
			
			persistData();
		}
	}
}
