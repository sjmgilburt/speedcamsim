package com.sjmgilburt.cloudcomputing;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URISyntaxException;
import java.security.InvalidKeyException;
import java.util.LinkedList;

import com.google.gson.Gson;
import com.microsoft.azure.storage.CloudStorageAccount;
import com.microsoft.azure.storage.StorageException;
import com.microsoft.azure.storage.queue.CloudQueue;
import com.microsoft.azure.storage.queue.CloudQueueClient;
import com.microsoft.azure.storage.queue.CloudQueueMessage;
import com.microsoft.azure.storage.table.CloudTable;
import com.microsoft.azure.storage.table.CloudTableClient;
import com.microsoft.azure.storage.table.TableBatchOperation;
import com.microsoft.windowsazure.Configuration;
import com.microsoft.windowsazure.services.servicebus.*;
import com.microsoft.windowsazure.services.servicebus.models.*;
import com.microsoft.windowsazure.core.*;
import com.microsoft.windowsazure.exception.ServiceException;

import javax.xml.datatype.*;

public class Monitor {
	private static final String SERVICE_BUS = "sjmg-cloudcomputing";
	private static final String TOPIC = "camera-message";
	private static final String SERVICE_PRIMARY_KEY = "5Aatn3q0TNeggXzwxlHACyZc3D6C8v1gZ/+Ja3bxRD8=";
	private static final String SPEEDER_SUBSCRIPTION = "speeders-sub";
	private static final String STORAGE_ACCOUNT = "resourcegp7343";
	private static final String STORAGE_KEY = "b4XhY4AAaWwMvwcCn0mKogYI44tvhig7ClW7K69ObV7qMlMz/80QKKFBowJfEwzkNlXg+cghNhbOSHmm7QkvoA==";
	private static final String STORAGE_CONNECTION_STRING = 
		    "DefaultEndpointsProtocol=http;" + 
		    "AccountName="+STORAGE_ACCOUNT+";" +
		    "AccountKey="+STORAGE_KEY;
	private static final String QUEUE_NAME = "scalable-queue";
	
	private static LinkedList<SSCMessage> messages = new LinkedList<SSCMessage>();
	
	private static void forwardMessage(String json) {
		try {
			CloudStorageAccount storageAccount = CloudStorageAccount.parse(STORAGE_CONNECTION_STRING);
			CloudQueueClient queueClient = storageAccount.createCloudQueueClient();
			CloudQueue queue = queueClient.getQueueReference(QUEUE_NAME);
			queue.createIfNotExists(); // creates queue (now technically unneccesary)
			
			queue.addMessage(new CloudQueueMessage(json));
		} catch (InvalidKeyException e) {
			e.printStackTrace();
		} catch (URISyntaxException e) {
			e.printStackTrace();
		} catch (StorageException e) {
			e.printStackTrace();
		}
	}
	
	private static LinkedList<SSCMessage> retrieveMessages(ServiceBusContract service) {
		LinkedList<SSCMessage> result = new LinkedList<SSCMessage>();
		try {
			ReceiveMessageOptions opts = ReceiveMessageOptions.DEFAULT;
			
			while(true) {
				ReceiveSubscriptionMessageResult resultSubMsg = service.receiveSubscriptionMessage(TOPIC, SPEEDER_SUBSCRIPTION, opts);
				BrokeredMessage message = resultSubMsg.getValue();
				
				if (message != null && message.getMessageId() != null) {
					String json = "";
					try {
						Reader inStrRd = new InputStreamReader(message.getBody());
						int data = -1;
						data = inStrRd.read();
						while (data != -1) {
							json += (char)data;
							data = inStrRd.read();
						}
					}
					catch (IOException e) {
						e.printStackTrace();
					}
					
					forwardMessage(json); // Forwards to queue for Vehicle Check application
					
					Gson gson = new Gson();
					SSCMessage sscm = gson.fromJson(json, SSCMessage.class);
					result.add(sscm);
				}
				else {
					break;
				}
			}
		}
		catch (ServiceException e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	private static void createTable(CloudTableClient client) {
		try {	
			CloudTable speedingVehicles = client.getTableReference("SpeedingVehicles");
			speedingVehicles.createIfNotExists();
		}
		catch (Exception e) {
		    e.printStackTrace();
		}
	}
	
	private static void sendAndPrint(CloudTableClient client) {
		CloudTable spdTable = null;
		try {
			spdTable = client.getTableReference("SpeedingVehicles");
		}
		catch (URISyntaxException e) {
			e.printStackTrace();
		}
		catch (StorageException e) {
			e.printStackTrace();
		}
		
		TableBatchOperation carBatch = new TableBatchOperation();
		TableBatchOperation bikeBatch = new TableBatchOperation();
		TableBatchOperation truckBatch = new TableBatchOperation();
		
		System.out.println("** NEW INSTANCES OF SPEEDING VEHICLES **");
		System.out.println("[REGISTRATION; SPEED; SPEED LIMIT; DATE/TIME; VEHICLE TYPE; LOCATION; CAMERA ID]");
		try {
			for (SSCMessage speeder : messages) {
				boolean priority = false;
				String tenPC = "";
				if (speeder.vehicle.speed > speeder.maxMPH*1.1) {
					priority = true;
					tenPC = "  PRIORITY";
				}
				
				VehicleEntity entity = new SpeedingVehicleEntity(speeder.vehicle, priority);
				if (entity.getType().equals("CAR"))	carBatch.insertOrReplace(entity);
				else if (entity.getType().equals("MOTORCYCLE")) bikeBatch.insertOrReplace(entity);
				else truckBatch.insertOrReplace(entity);
				
				System.out.println(speeder.vehicle.reg + "; " +
								speeder.vehicle.speed + "; " +
								speeder.maxMPH + "; " +
								speeder.vehicle.dateTime + "; " +
								speeder.vehicle.type + "; " +
								speeder.street + ", " + speeder.city + "; " +
								speeder.sscID +
								tenPC);
			}
			
				
			if (!carBatch.isEmpty())spdTable.execute(carBatch);
			if (!bikeBatch.isEmpty()) spdTable.execute(bikeBatch);
			if (!truckBatch.isEmpty()) spdTable.execute(truckBatch);
		}
		catch (StorageException e) {
			e.printStackTrace();
		}
		
		messages = new LinkedList<SSCMessage>();
		System.out.println();
	}

	public static void main(String[] args) {
		Configuration config =
			    ServiceBusConfiguration.configureWithSASAuthentication(
			      SERVICE_BUS,
			      "RootManageSharedAccessKey",
			      SERVICE_PRIMARY_KEY,
			      ".servicebus.windows.net"
			      );
		ServiceBusContract service = ServiceBusService.create(config);
		
		/** SUBSCRIPTION NOW EXISTS, THROWS EXCEPTION/WARNING IF SETUP RERUN */
		/*
		SubscriptionInfo subInfo = new SubscriptionInfo(SPEEDER_SUBSCRIPTION);
		try {
			CreateSubscriptionResult result = service.createSubscription(TOPIC, subInfo);
		
			RuleInfo ruleInfo = new RuleInfo("speedrule");
			ruleInfo = ruleInfo.withSqlExpressionFilter("Speeding = true");
			
			CreateRuleResult ruleResult = service.createRule(TOPIC, SPEEDER_SUBSCRIPTION, ruleInfo);
			service.deleteRule(TOPIC, SPEEDER_SUBSCRIPTION, "$Default");
		}
		catch (ServiceException e) {
			e.printStackTrace();
		}
		*/
		
		CloudStorageAccount storageAccount = null;
		CloudTableClient tableClient = null;
		try {
			storageAccount = CloudStorageAccount.parse(STORAGE_CONNECTION_STRING);
			tableClient = storageAccount.createCloudTableClient();
		
			createTable(tableClient);
		}
		catch (InvalidKeyException e) {
			e.printStackTrace();
		}
		catch (URISyntaxException e) {
			e.printStackTrace();
		}
		
		while (true) {
			messages  = retrieveMessages(service);
			sendAndPrint(tableClient);
		}
	}

}
