package com.sjmgilburt.cloudcomputing;

import java.net.URISyntaxException;
import java.security.InvalidKeyException;

import com.microsoft.azure.storage.*;
import com.microsoft.azure.storage.table.*;
import com.microsoft.azure.storage.table.TableQuery.*;

import java.sql.*;

import com.microsoft.sqlserver.jdbc.*;

public class Querier {
	private static final String STORAGE_ACCOUNT = "resourcegp7343";
	private static final String STORAGE_KEY = "b4XhY4AAaWwMvwcCn0mKogYI44tvhig7ClW7K69ObV7qMlMz/80QKKFBowJfEwzkNlXg+cghNhbOSHmm7QkvoA==";
	private static final String STORAGE_CONNECTION_STRING = 
		    "DefaultEndpointsProtocol=http;" + 
		    "AccountName="+STORAGE_ACCOUNT+";" +
		    "AccountKey="+STORAGE_KEY;
	private static final String CAMERA_TABLE_NAME = "cameras";
	private static final String VEHICLE_TABLE_NAME = "vehicles";
	private static final String SPEEDER_TABLE_NAME = "SpeedingVehicles";
	private static final String PWORD = "acses-987";
	
	static void sscRegistrations(CloudTableClient client) {
		try {
			CloudTable camTable = client.getTableReference(CAMERA_TABLE_NAME);
		
			System.out.println("** SMART SPEED CAMERA REGISTRATIONS **");
			System.out.println("[ID; STREET; CITY; SPEED LIMIT]");
			for (CameraEntity entity : camTable.execute(TableQuery.from(CameraEntity.class))) {
				System.out.println(entity.getID() + "; " + 
								entity.getStreet() + "; " + 
								entity.getCity() + "; " + 
								entity.getMaxMPH());
			}
		}
		catch (URISyntaxException e) {
			e.printStackTrace();
		}
		catch (StorageException e) {
			e.printStackTrace();
		}
		System.out.println();
	}
	
	static void speederSightings(CloudTableClient client) {
		try {
			CloudTable spdTable = client.getTableReference(SPEEDER_TABLE_NAME);
			CloudTable vehTable = client.getTableReference(VEHICLE_TABLE_NAME);
			CloudTable camTable = client.getTableReference(CAMERA_TABLE_NAME);
			
			// Output headers		
			System.out.println("** PRIORITY VEHICLE SIGHTINGS **");
			System.out.println("\t[DATE/TIME; SPEED; SPEED LIMIT; CAMERA ID; STREET; CITY]");
			
			// Find all priority speeding vehicles
			for (SpeedingVehicleEntity spdVehEnt : spdTable.execute(
					TableQuery.from(SpeedingVehicleEntity.class).where(
							TableQuery.generateFilterCondition("Priority", QueryComparisons.EQUAL, true)))) {
				
				// Print out registration (header)
				System.out.println("[" + spdVehEnt.getReg() + ":]");
				
				// Find all sightings 
				for (VehicleEntity vehEnt : vehTable.execute(
						TableQuery.from(VehicleEntity.class).where(
								TableQuery.generateFilterCondition("Reg", QueryComparisons.EQUAL, spdVehEnt.getReg())))) {
					// Find an instance of a matching camera ID for the speed limit and location details (breaks once found)
					for (CameraEntity camEnt : camTable.execute(
							TableQuery.from(CameraEntity.class).where(
									TableQuery.generateFilterCondition("ID", QueryComparisons.EQUAL, vehEnt.getSSCID())))) {
						// Print out historical sighting details
						System.out.println("\t" + 
										vehEnt.getDateTime() + "; " +
										vehEnt.getSpeed() + "; " + 
										camEnt.getMaxMPH() + "; " + 
										camEnt.getID() + "; " + 
										camEnt.getStreet() + "; " + 
										camEnt.getCity());
						break;
					}
				}
			}
		}
		catch (URISyntaxException e) {
			e.printStackTrace();
		}
		catch (StorageException e) {
			e.printStackTrace();
		}
		System.out.println();
	}
	
	static void stolenVehiclesSQL() {
		String connectionString = "jdbc:sqlserver://zirm8u53ce.database.windows.net:1433;"
				+ "database=sql-database;"
				+ "user=admin-login@zirm8u53ce;"
				+ "password="+PWORD+";"
				+ "encrypt=true;"
				+ "hostNameInCertificate=*.database.windows.net;"
				+ "loginTimeout=30;";
		
		try {
			Connection connection = DriverManager.getConnection(connectionString);
			String selectSql = "SELECT reg, stolen FROM dbo.Checks";
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery(selectSql);
			
			System.out.println("** STOLEN VEHICLES **");
			System.out.println("[REGISTRATION; STOLEN STATUS]");
			while (resultSet.next()) {
			    System.out.println(resultSet.getString(1) + "; " + resultSet.getBoolean(2));
			}
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		System.out.println();
	}

	public static void main(String[] args) {
		CloudStorageAccount storageAccount;
		CloudTableClient tableClient = null;
		try {
			storageAccount = CloudStorageAccount.parse(STORAGE_CONNECTION_STRING);
			tableClient =  storageAccount.createCloudTableClient();
		}
		catch (InvalidKeyException e) {
			e.printStackTrace();
		}
		catch (URISyntaxException e) {
			e.printStackTrace();
		}

		sscRegistrations(tableClient);
		speederSightings(tableClient);
		
		stolenVehiclesSQL();
	}

}
