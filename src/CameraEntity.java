package com.sjmgilburt.cloudcomputing;

import java.util.Date;

import com.microsoft.azure.storage.*;
import com.microsoft.azure.storage.table.*;
import com.microsoft.azure.storage.table.TableQuery.*;

public class CameraEntity extends TableServiceEntity {
	private int id;
	private String street;
	private String city;
	private int maxMPH;
	private Date startTime;
	
	public CameraEntity() {} // Nullary constructor required
	
	public CameraEntity(SSCMessage sscm) {
		this.partitionKey = sscm.city;
        this.rowKey = Integer.toString(sscm.sscID) + ", " + sscm.startTime.toString();
        
        id = sscm.sscID;
        this.street = sscm.street;
        this.city = sscm.city;
        this.maxMPH = sscm.maxMPH;
        this.startTime = sscm.startTime;
	}
	
	public CameraEntity(int id, String street, String city, int maxMPH, Date startTime) {
        this.partitionKey = city;
        this.rowKey = Integer.toString(id) + ", " + startTime.toString();
        
        this.id = id;
        this.street = street;
        this.city = city;
        this.maxMPH = maxMPH;
        this.startTime = startTime;
    }

    public int getID() { 
		return id;
	}
	
	public String getStreet() {
		return street;
	}
	
	public String getCity() {
		return city;
	}
	
	public int getMaxMPH() {
		return maxMPH;
	}
	
	public Date getStartTime() {
		return startTime;
	}
	
	 public void setID(int id) { 
			this.id = id;
		}
		
	public void setStreet(String street) {
		this.street = street;
	}
	
	public void setCity(String city) {
		this.city = city;
	}
	
	public void setMaxMPH(int maxMPH) {
		this.maxMPH = maxMPH;
	}
	
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}
}
