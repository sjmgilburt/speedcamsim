package com.sjmgilburt.cloudcomputing;

import java.util.Date;

public class SSCMessage {
	public int sscID;
	public String street;
	public String city;
	public int maxMPH;
	public Date startTime;
	
	public Vehicle vehicle;
	
	public SSCMessage(int id, String street, String city, int maxMPH, Date startTime) {
		// For Startup notification ONLY		
		sscID = id;
		this.street = street;
		this.city = city;
		this.maxMPH = maxMPH;
		this.startTime = startTime;
		vehicle = null;
	}
	
	public SSCMessage(int id, String street, String city, int maxMPH, Date startTime, Vehicle vehicle) {
		sscID = id;
		this.street = street;
		this.city = city;
		this.maxMPH = maxMPH;
		this.startTime = startTime;
		this.vehicle = vehicle;
	}
	
	public String toString() {
		if (vehicle == null) {
			return "[ " + sscID +
					", " + street +
					", " + city +
					", " + maxMPH +
					", " + startTime.toString() +
					", STARTUP NOTIFICATION ]";
		}
		else return "[ " + sscID +
					", " + street +
					", " + city +
					", " + maxMPH +
					", " + startTime.toString() +
					", " + vehicle.toString() + " ]";
	}
	
	public boolean hasVehicle() {
		return (vehicle != null);
	}
}