package com.sjmgilburt.cloudcomputing;

import java.net.URISyntaxException;
import java.nio.file.WatchEvent.Kind;
import java.security.InvalidKeyException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Scanner;

import com.microsoft.azure.storage.CloudStorageAccount;
import com.microsoft.azure.storage.StorageException;
import com.microsoft.azure.storage.queue.CloudQueue;
import com.microsoft.azure.storage.queue.CloudQueueClient;
import com.microsoft.azure.storage.queue.CloudQueueMessage;
import com.microsoft.azure.storage.table.CloudTable;
import com.microsoft.azure.storage.table.CloudTableClient;
import com.microsoft.azure.storage.table.TableOperation;
import com.microsoft.windowsazure.Configuration;
import com.microsoft.windowsazure.services.servicebus.*;
import com.microsoft.windowsazure.services.servicebus.models.*;
import com.microsoft.windowsazure.core.*;
import com.microsoft.windowsazure.exception.ServiceException;

import javax.xml.datatype.*;

import java.sql.*;

import com.microsoft.sqlserver.jdbc.*;

public class App {
	private static final String STORAGE_ACCOUNT = "resourcegp7343";
	private static final String STORAGE_KEY = "b4XhY4AAaWwMvwcCn0mKogYI44tvhig7ClW7K69ObV7qMlMz/80QKKFBowJfEwzkNlXg+cghNhbOSHmm7QkvoA==";
	private static final String STORAGE_CONNECTION_STRING = 
		    "DefaultEndpointsProtocol=http;" + 
		    "AccountName="+STORAGE_ACCOUNT+";" +
		    "AccountKey="+STORAGE_KEY;
	private static final String CAMERA_TABLE_NAME = "cameras";
	private static final String VEHICLE_TABLE_NAME = "vehicles";
	private static final String QUEUE_NAME = "scalable-queue";
	private static final String PWORD = "acses-987";
	
	/** Main method class used for testing/setting up cloud structures during development */
	public static void main( String[] args ) {
		/*
		CloudStorageAccount storageAccount = null;
		CloudTableClient tableClient = null;
		CloudQueueClient queueClient = null;
		CloudQueue queue = null;
		try {
			storageAccount = CloudStorageAccount.parse(STORAGE_CONNECTION_STRING);
			tableClient = storageAccount.createCloudTableClient();
			queueClient = storageAccount.createCloudQueueClient();
			queue = queueClient.getQueueReference(QUEUE_NAME);
			//queue.createIfNotExists(); // supposedly creates queue, I can't see it
			
			CloudQueueMessage message = new CloudQueueMessage("test message");
			queue.addMessage(message);		    
		    
		    // Output the message value.
		    while (true) {
		    	CloudQueueMessage msg = queue.retrieveMessage();
		    	queue.downloadAttributes();
		    	
		    	if (msg != null) {
			    	System.out.println(msg.getMessageContentAsString());
			    	System.out.println(queue.getApproximateMessageCount());
			    	queue.deleteMessage(msg);
		    	}
		    	else break;
		    }
		}
		catch (InvalidKeyException e) {
			e.printStackTrace();
		}
		catch (URISyntaxException e) {
			e.printStackTrace();
		}
		catch (StorageException e) {
			e.printStackTrace();
		}
		*/
		
		/* Used to manually add vehicles and cameras to table
		CloudTable camTable = null;
		CloudTable vehTable = null;
		try {
			camTable = tableClient.getTableReference(CAMERA_TABLE_NAME);
			vehTable = tableClient.getTableReference(VEHICLE_TABLE_NAME);
		}
		catch (URISyntaxException e) {
			e.printStackTrace();
		}
		catch (StorageException e) {
			e.printStackTrace();
		}
		
		CameraEntity testCamera = new CameraEntity(6653, "The Causeway", "Billingham", 30, new Date());
		VehicleEntity testCar = new VehicleEntity("LA06 VVS", Vehicle.Type.TRUCK, 26.6546735158, new Date(), 6653);
		try {
			camTable.execute(TableOperation.insertOrReplace(testCamera));
			vehTable.execute(TableOperation.insertOrReplace(testCar));
		}
		catch (StorageException e) {
			e.printStackTrace();
		}
		*/
		
		/*
		String connectionString = "jdbc:sqlserver://zirm8u53ce.database.windows.net:1433;"
				+ "database=sql-database;"
				+ "user=admin-login@zirm8u53ce;"
				+ "password="+PWORD+";"
				+ "encrypt=true;"
				+ "hostNameInCertificate=*.database.windows.net;"
				+ "loginTimeout=30;";

		try {
			Connection connection = DriverManager.getConnection(connectionString);
			
			PreparedStatement create = connection.prepareStatement("CREATE TABLE Checks (reg VARCHAR(32) PRIMARY KEY, stolen BIT NOT NULL);");
			
			create.execute();
		
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		*/
	}
}
