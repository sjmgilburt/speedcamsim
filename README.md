# Speed Camera Simulation

This is a personal project developed by Samuel J. M Gilburt in order to experiment with message brokering and NoSQL 
consumption in the cloud, via Microsoft Azure.

## Summary

The project simulates a network of "smart speed cameras" that detect speeding cars in various locations, and report the 
details to an Azure Service Bus for processing. These reports are processed by a server-side NoSQL consumer and stored 
in a non-relational Azure Table. This consumer also registers new cameras into the system. Speeding reports are also 
consumed by a server-side police monitor program which filters the data based on whether a car was speeding, and 
reports speeding vehicles to the terminal and stores the details in a non-relational Azure Table. Finally, there is a 
query application which can list registered speed cameras and track historical sightings of speeding vehicles.

Also included in the project is a vehicle check program which was developed with the aim of experimenting with cloud 
auto-scaling in Azure. The program simulates a laborious vehicle check process and when run server-side, processes 
output from the police monitor and can be configured in Azure to auto-scale to demand in an Azure Queue.

The component parts of the project are explained in further detail below.

## Simulation

### SmartSpeedCamera

The *SmartSpeedCamera* class contains class variables for a unique identifier (*id*), street name (*street*), town/city 
(*city*) and maximum speed limit for the area it monitors (*maxMPH*). There is also a class variable to indicate the 
date and time that the camera started up (*startTime*), and a buffer for outgoing messages (*outBuffer*).

Upon launch, the program prompts the user for the file name/path of the .txt format configuration file and prompts for 
the vehicles per minute rate. The constructor initialises the class variables based on the config file contents and 
adds an *[SSCMessage](#sscmessage)* to the buffer ready to be sent to inform the Azure Service Bus Topic that it has 
started up. It then attempts to send this message via the *[send()](#send)* method.

At the rate specified, the program then endlessly simulates vehicles passing the camera using the *vehiclePass()* 
method. This creates a new *[Vehicle](#vehicle)* class, adds it to the buffer and then attempts to send the message via 
the *[send()](#send)* method.

#### send()

*send()* first uses the *connection()* method to see if there is an internet connection to the server – if this check 
fails the message remains stored in the buffer. The method connects to the service bus and sends all messages currently 
in the buffer to the topic. The message is an *[SSCMessage](#sscmessage)* serialised using JSON. The method also adds a 
"Speeding" property to the message to indicate if the vehicle was speeding.

### SSCMessage

*SSCMessage* is a class which contains all of the class variable details from the sending 
*[SmartSpeedCamera](#smartspeedcamera)* class, as well as an optional *[Vehicle](#vehicle)* class variable (*vehicle*).
It includes a *hasVehicle()* method to determine whether *vehicle* has been given a value (i.e. whether the message is 
a camera startup notification or a vehicle sighting).

### Vehicle

*Vehicle* is a class that represents a vehicle that passes a smart speed camera. It contains class variables for the 
vehicle registration plate (*reg*), vehicle type (an enum, *type*), current speed of travel (*speed*) and the ID of the 
camera that made the sighting (*sscID*). It also contains a variable to log the time it was seen (*dateTime*). Upon 
construction, the registration is generated randomly to realistically represent a plate format from 1983 to 2016, type 
is selected randomly, speed generated depending on the speed limit of the camera (passed as a constructor parameter) 
and the remaining class variables assigned accordingly.

## Message Consumption

### Consumer

The *Consumer* class program runs endlessly, periodically checking the queue for incoming messages by way of the 
*[poll()](#poll)* method. The program then stores the polled messages for each round in the only class variable, 
*messages*, a list. If messages have been retrieved successfully, the program calls the *[addToTables()](#addtotables)* 
method in order to persist the data in the appropriate Azure Table Storage tables.

As the Azure platform charges per request, there is a motivation to reduce the request quantity. One method is to 
communicate with the cloud services in batches as described above. Another method incorporated into the program is to 
vary the interval between polls, like so:
* Initially poll once every 10 seconds
* If messages present, halve polling interval
* If messages absent, increase polling interval by half
* Poll once every 5 minutes minimum
* Poll once every second maximum

#### poll()

The *poll()* method attempts to pop messages from the queue, and if there are messages available, retrieves all in a 
batch. Each message is decoded in order to obtain the full *[SSCMessage](#sscmessage)* sent, which is then added to a 
list returned as the result of the method.

#### addToTables()

*addToTables()* cycles through the messages list determining if the message represents a camera startup notification or 
a vehicle pass. If it is a camera message, a new *[CameraEntity](#cameraentity-and-vehicleentity)* is created with the 
camera properties and added to the Azure table "cameras". If it is a vehicle message, a new 
*[VehicleEntity](#cameraentity-and-vehicleentity)* is created with the vehicle properties which is added to a batch 
according to the vehicle type (the partition key). Once all vehicles are added, the batches are then added to the Azure 
table "vehicles".

### CameraEntity and VehicleEntity

*CameraEntity* and *VehicleEntity* are classes which extend Azure’s 
*[TableServiceEntity](https://azure.github.io/azure-sdk-for-java/com/microsoft/azure/storage/table/TableServiceEntity.html)* 
class – storing the camera and vehicle details in such a way that they may be stored in their respective tables and 
providing the required constructors, getters and setters for each.

### Monitor

The *Monitor* class program retrieves messages in essentially the same way as the *[Consumer](#consumer)* class. 
However, it utilises an Azure Subscription which filters out messages where a "Speeder" property has not been set to 
true (as implemented in the *[SmartSpeedCamera](#smartspeedcamera)* class program). The program runs endlessly, polling 
the Subscription for messages and printing out the details of the speeding vehicles to the console each round (note 
that the headers will print regardless of vehicles found).

As speed cameras may be inaccurate by up to 10%, the program determines whether the speeding vehicle is going 110% of 
the speed limit or more, in which case a variable *tenPC* is updated to append "PRIORITY" to the output for any 
vehicles where this is the case.

The program adds *[SpeedingVehicleEntity](#speedingvehicleentity)* classes to the Azure table "SpeedingVehicles". 

### SpeedingVehicleEntity

*SpeedingVehicleEntity* extends *[VehicleEntity](#cameraentity-and-vehicleentity)* in order to allow for the *priority* 
variable to be stored alongside the rest of the vehicle details in the table.

### VehicleCheck

The *VehicleCheck* class program simulates a time-consuming method for checking if a speeding vehicle has been stolen. 
It retrieves messages from an Azure Queue that have been forwarded by the *[Monitor](#monitor)* class program, in 
effectively the same manner as the *[Consumer](#consumer)* and *[Monitor](#monitor)* classes (continually polling). 
Each time it retrieves a vehicle message, however, it runs the *isVehicleStolen()* method, which causes a 5 second 
delay. If there is any volume of messages higher than this being forwarded by the *[Monitor](#monitor)* class program, 
the *VehicleCheck* program is unable to keep up.

As the *VehicleCheck* class program retrieves messages from an Azure Queue and not a Service Bus Topic, it is 
subsequently auto-scalable using Azure. Adding a number of virtual machines to an availability set and running the 
program as an executable .jar file on the virtual machine allows the Queue to automatically call additional VMs to 
start running in order to deal with the demand of the queue, should it require it (and likewise scale down when the 
demand reduces).

As the *VehicleCheck* class program reads in each message, it stores the registration plate and stolen status as an 
SQL-compatible string in the only class variable, *resultsSQL*, a list. Once it has exhausted the queue of messages, it 
calls the *persistData()* method. This method connects to an SQL database stored on Azure, and runs an SQL "INSERT" 
statement with all values held in *resultsSQL* in order to add the data to a table called "Checks".

## Data Retrieval

### Querier

The *Querier* class program provides three data queries (as detailed below), which are executed in succession, printing 
their outputs to the console.

#### sscRegistrations()

The first query prints all smart speed camera registrations using the *sscRegistrations()* method. This method simply 
loops through the "cameras" table, printing the ID, street, city and speed limit for each camera.

#### speederSightings()

The second query prints all historical sightings of priority speeding vehicles using the *speederSightings()* method. 
This method retrieves all priority speeding vehicles from the "SpeedingVehicles" table. For each record, it retrieves 
all sightings from the "vehicles" table that match the registration of the vehicle in question. Cross-referencing with 
the "cameras" table for the speed limit and location data, it then prints out details of each sighting of the vehicle – 
with date/time, speed, speed limit, camera ID, street and city.

#### stolenVehiclesSQL()

The final query prints all vehicles from the "Checks" table of the Azure SQL database using the *stolenVehiclesSQL()* 
method. This method uses an SQL "SELECT" query and then prints out the resultant registrations with their corresponding 
stolen status value (true or false).

## Built with

* [Microsoft Azure](https://azure.microsoft.com) - cloud service for simulation

## Acknowledgements

* [Tom Fricker](https://thenounproject.com/tom.fricker/) - project icon